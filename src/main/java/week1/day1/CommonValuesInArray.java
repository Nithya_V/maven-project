package week1.day1;

public class CommonValuesInArray {
 // To print common values present in two array
	public static void main(String[] args) {
		int[] box1 = {1, 4, 5, 9, 7, 10, 15};
		int[] box2 = {7, 2, 3, 10, 12};
		int count = 0;
		for(int i=0; i<box1.length; i++) {
			for(int j=0; j<box2.length; j++) {
				if(box1[i] == box2[j]) {
					count = count + 1;
					System.out.println("Common value is: " + box1[i]);
								}
			}
		}
		
		System.out.println("No of common values : " + count);	
	}

}
