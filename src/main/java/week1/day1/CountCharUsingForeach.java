package week1.day1;

public class CountCharUsingForeach {
  //  to find the no of char 'e' present in the given string
	public static void main(String[] args) {
		String company = "Testleaf";
		int count = 0;
	    char[] charArray = company.toCharArray();
		for(char cname : charArray) {
			//System.out.println(cname);		
			if (cname=='e')	{				
				count = count + 1;
			}
			
	    }
				
	    System.out.println("No of char 'e' present is: "+count);
			}
			
		}


