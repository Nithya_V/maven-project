package week1.day1;

public class CountCharUsingReplaceAll {
   //  to find the no of char 'e' present in the given string using ReplaceAll
	public static void main(String[] args) {
		String company = "Testleaf";
		String replaceAll = company.replaceAll("[^e]", "");
		System.out.println("No of char 'e' present is: " + replaceAll.length());
				
	}

}
