package week1.day1;

public class OddIndexToUppercase {

	public static void main(String[] args) {
		// To change all odd index to uppercase
		String word = "changeme";
		//char[] charArray = word.toCharArray();
        for(int i=0; i < word.length(); i++) {
        	char charAt = word.charAt(i);
        	if (i%2 == 0) {
            System.out.print(Character.toLowerCase(charAt));
        	}
        	else {
        		System.out.print(Character.toUpperCase(charAt));
        	}
        }
	}

}
