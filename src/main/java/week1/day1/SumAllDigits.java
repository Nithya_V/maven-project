package week1.day1;

public class SumAllDigits {

	public static void main(String[] args) {
		// To sum all the digits in the given number
		//to separate the digits, we need to find reminders by dividing by 10
		int num = 1546;
		int sum = 0;
		int rem = 0;
	    while(num > 0) {
	    	rem = num % 10;
	    	sum = sum + rem;
	    	num = num/10;    
	    			
		}
		
		System.out.println("Sum of given digits:" + sum);

	}

}
