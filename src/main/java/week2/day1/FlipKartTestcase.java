package week2.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class FlipKartTestcase {

	public static void main(String[] args) throws InterruptedException {
		// FlipKart testcase to cick on Mi and print all phone name with their corresponding price
		//Set the driver path
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//launch chrome browser
		ChromeDriver driver = new ChromeDriver();
		//implicit wait
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//maximize browser window
		driver.manage().window().maximize();
		//open flipkart url
		driver.get("https://www.flipkart.com/");
		driver.findElementByXPath("//button[text()='✕']").click();
		WebElement Electronics = driver.findElementByXPath("//span[text()='Electronics']");
		Actions builder = new Actions(driver);
		builder.moveToElement(Electronics).perform();
		driver.findElementByXPath("//a[text()='Mi']").click();
		Thread.sleep(2000);
		String s= "Mi Mobile";
		String pagetitle = driver.getTitle();
		System.out.println(pagetitle);
		if(pagetitle.contains(s)) {
			System.out.println("Title contains: " + s);
		}
		else {
			System.out.println("Title does not contain: " + s);
		}


		List<WebElement> mobile = driver.findElements(By.xpath("//div[@class='_3wU53n']"));
		int mobilecount = mobile.size();

		if(mobilecount>0)
		{
			System.out.println("Total No of mobile displayed is : " +mobilecount);
			for (int i=1; i<=mobilecount; i++) 
			{
				String mobilename = driver.findElement(By.xpath("(//div[@class='_3wU53n'])["+ i +"]")).getText();
				String mobilePrice = driver.findElement(By.xpath("(//div[@class='_1vC4OE _2rQ-NK'])["+ i +"]")).getText();
				System.out.println(+i +". Mobile Name is :" +mobilename +" and its Price is :" +mobilePrice);
			}
		}
		else
			System.out.println("Result displayed for mobile : " +mobilecount);


	}
}


