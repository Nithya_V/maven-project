package week2.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnTables {

	public static void main(String[] args) {
		//Set the driver path
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//launch chrome browser
		ChromeDriver driver = new ChromeDriver();
		//implicit wait
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//maximize browser window
		driver.manage().window().maximize();
		//open IRCTC registration url
		driver.get("https://erail.in/");
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("CBE",Keys.TAB);
        WebElement table = driver.findElementById("DataTable TrainList TrainListHeader");
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        System.out.println(rows.size());
        WebElement firstrow = rows.get(0);
        List<WebElement> columns = firstrow.findElements(By.tagName("td"));
        System.out.println(columns.size());
        String text = columns.get(1).getText();
        System.out.println("Train name: " + text);
        driver.close();
	}

}
