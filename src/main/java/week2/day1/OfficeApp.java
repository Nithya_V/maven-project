package week2.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class OfficeApp {

	public static void main(String[] args) throws InterruptedException {
		// FlipKart 
		//Set the driver path
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//launch chrome browser
		ChromeDriver driver = new ChromeDriver();
		//implicit wait
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//maximize browser window
		driver.manage().window().maximize();
		//open flipkart url
	    driver.get("https://www.cnn.com/");
	    //driver.findElementById("menu").click();
	    //driver.findElementByPartialLinkText("more").click();
	    Thread.sleep(2000);
	    driver.findElementByXPath("//form[@id='search-form']/div[2]").click();
	    driver.findElementById("search-input-field").click();
	    driver.findElementById("search-input-field").sendKeys("travel");
	    driver.findElementByXPath("//div[@class='search__form-fields']/button").click();
	    driver.findElementByXPath("//label[text()='Stories']").click();
	    String searchresult = driver.findElementByXPath("//div[@class='cnn-search__results-count']").getText();
	    String searchinput = "travel";
	    if(searchresult.contains(searchinput)) {
	    	System.out.println("Search result contains : " + searchinput);
	    }
	    else {
	    	System.out.println("Search result not contains : " + searchinput );
	    	
	    }
	    //WebElement Date = driver.findElementByXPath("//li[text()='Date']");
	    
	    
	    
	    
	    

	}

}
