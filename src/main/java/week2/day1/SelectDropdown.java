package week2.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SelectDropdown {

	public static void main(String[] args) {
		// To select country name starting with 'E' but the second value 'Egypt'
		//Set the driver path
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//launch chrome browser
		ChromeDriver driver = new ChromeDriver();
		//implicit wait
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//maximize browser window
		driver.manage().window().maximize();
		//open IRCTC registration url
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		int count = 0;
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select countrydd = new Select(country);
		List<WebElement> options = countrydd.getOptions();
		for(WebElement ddvalue:options) {
			if(ddvalue.getText().startsWith("E")) {
				count++;
				if(count==2) {
					countrydd.selectByVisibleText(ddvalue.getText());
					break;
				}
			}
			
		}

	}

}
