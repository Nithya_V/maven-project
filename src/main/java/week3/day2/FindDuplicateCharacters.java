package week3.day2;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class FindDuplicateCharacters {

	public static void main(String[] args) {
		// Given string find out all the duplicate characters
		String S = "Infosys Limited";
		char[] ca = S.toCharArray();
		Map<Character, Integer> map1 = new LinkedHashMap<>();
		for(char c : ca) {
			//Integer countt = map1.get(c);
			//System.out.println("Test : "+countt);
			if(map1.containsKey(c)) {
			int count = map1.get(c);
			map1.put(c, (map1.get(c))+1);
			//System.out.print(map1.get(c));
			}
			else {
				map1.put(c, 1);
			}
				
		}
		for(char t : map1.keySet()) {
			if (map1.get(t) > 1) {
				System.out.println("duplicate char :" + t);
			}
		}
		}

	}


