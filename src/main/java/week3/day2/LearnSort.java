package week3.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LearnSort {

	public static void main(String[] args) {
		// Sort the given company in reverse order
		List<String> cname = new ArrayList<>();
		cname.add("HCL");
		cname.add("CTS");
		cname.add("Aspire Systems");
		//cname.add("Asphalt");
	    Collections.sort(cname);
	    System.out.println(cname);
 
	}

}
