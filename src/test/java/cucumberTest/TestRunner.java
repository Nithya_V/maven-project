package cucumberTest;

import org.testng.annotations.Test;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import cucumber.api.testng.TestNGCucumberRunner;

@CucumberOptions(features="src/test/java/feature/Login.feature", glue="stepDefinition", monochrome=true)
public class TestRunner extends AbstractTestNGCucumberTests{

}
