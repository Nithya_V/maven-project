package cucumberTest;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features="src/test/java/feature/Leaftaps Login.feature", glue="stepDefinitionForLeaftaps", monochrome=true)
public class TestRunner_Leaftaps extends AbstractTestNGCucumberTests{

}
