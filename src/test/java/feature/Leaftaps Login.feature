Feature: Leaftaps Login
Background:
Given Launch the Browser
And Maximise the Browser
And Set the Timeouts
And Load the URL

Scenario Outline: TC001 Positive Login Flow
Given Enter the Username as <username>
And Enter the Password as <password>
When I click on the Login Button
Then Verify Login is Success

Examples:
|username|password|
|Demosalesmanager|crmsfa|
|DemoCSR|crmsfa|

Scenario: TC002 Negetive Login Flow
Given Enter the Username as DemoCSR1
And Enter the Password as crmsfa
When I click on the Login Button
But Verify Login is Failure
