Feature: Login Action

Scenario: Successful Login with Valid Credentials
Given Launch the Browser
And Maximise the Browser
And Set the Timeouts 
And User is on Login page
When User enters Username
And User enters Password
And Click on Login button
Then User is successfully logged in