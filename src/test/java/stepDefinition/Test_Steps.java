package stepDefinition;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Test_Steps {
	public ChromeDriver driver;
	
	@Given("Launch the Browser")
	public void launchTheBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	  
	}

	@And("Maximise the Browser")
	public void maximiseTheBrowser() {
		driver.manage().window().maximize();
	}

	@And("Set the Timeouts")
	public void setTheTimeouts() {
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}
	
	@Given("User is on Login page")
	public void userIsOnLoginPage() {
		driver.get("https://app.onesignal.com/login");
	}

	@When("User enters Username")
	public void userEntersUsername() {
		driver.findElementByXPath("//input[@name='user[email]']").sendKeys("Nithya.Venkatesh@turner.com");
	}

	@When("User enters Password")
	public void userEntersPassword() {
		driver.findElementByXPath("//input[@name='user[password]']").sendKeys("Encourage@04");
	}

	@When("Click on Login button")
	public void clickOnLoginButton() {
		driver.findElementByXPath("//span[text()='Log In']").click();
	}

	@Then("User is successfully logged in")
	public void userIsSuccessfullyLoggedIn() {
		System.out.println("Login is Successful");
	}
}
