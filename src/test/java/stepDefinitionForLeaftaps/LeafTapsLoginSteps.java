package stepDefinitionForLeaftaps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.But;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LeafTapsLoginSteps {
	public ChromeDriver driver;
	@Given("Launch the Browser")
	public void launchBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}
	@And("Maximise the Browser")
	public void maxBrowser() {
		driver.manage().window().maximize();
	}
	
	@And("Set the Timeouts")
	public void setTimeouts() {
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}
	@And("Load the URL")
	public void loadURL() {
		driver.get("http://leaftaps.com/opentaps/");
	}
	@And("Enter the Username as (.*)")
	public void enterUserName(String data) {
		driver.findElementById("username").sendKeys(data);
	}
	@And("Enter the Password as (.*)")
	public void enterPassword(String data) {
		driver.findElementById("password").sendKeys(data);
	}
	@When("I click on the Login Button")
	public void clickLogin() {
		driver.findElementByClassName("decorativeSubmit").click();
	}
	@Then("Verify Login is Success")
	public void VerifyLogin() {
		System.out.println("Verify Login is Success");
	}
	@But("Verify Login is Failure")
	public void failureLogin() {
		System.out.println("Verify Login is Failure");
	}
}
